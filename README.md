# CSS3 Animation

#### 介绍
主要包括了几个简单的使用css3实现的动画示例
1. css红包雨
2. js实现图片跑马灯
3. transition实现轮播效果

#### 软件架构
分为html,css,js,img四个文件夹


#### 使用说明

三个示例依次是：

1.  rain.html
2.  marquee.html
3.  swiper.html

支持浏览器:point_right: ：**IE, FireFox**

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

